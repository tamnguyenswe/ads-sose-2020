/*************************************************
* ADS Praktikum 2.2
* TreeNode.cpp
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
#include "TreeNode.h"
#include <iostream>
#include <string>


////////////////////////////////////
// Ihr Code hier:

void fillSpace(int n);


TreeNode::TreeNode(int nodePosId, int nodeId, std::string &name, int alter, double einkommen, int plz) {

    this->name = name;
    this->alter = alter;
    this->einkommen = einkommen;
    this->plz = plz;
    this->nodePosID = nodePosId;
    this->nodeID = nodeId;

    this->color = RED; // new node always red
}


int TreeNode::getNodePosID() const {
    return nodePosID;
}


int TreeNode::getNodeID() const {
    return nodeID;
}


std::string TreeNode::getName() const {
    return name;
}


int TreeNode::getAlter() const {
    return alter;
}


double TreeNode::getEinkommen() const {
    return einkommen;
}


int TreeNode::getPLZ() const {
    return plz;
}


bool TreeNode::getRed() const {
    return this->color;
}


TreeNode *TreeNode::getLeft() const {
    return left;
}


TreeNode *TreeNode::getRight() const {
    return right;
}


TreeNode * TreeNode::getParent() const {
    return this->parent;
}


/**
 * @return Sibling node of this node's parent, if existed. Otherwise return Tree.nil node.
 */
TreeNode * TreeNode::getUncle() const {
    TreeNode * grandParent = this->getGrandparent();

    if (parent->isRightChild())
        return grandParent->getLeft();
    else
        return grandParent->getRight();
}


/**
 * @return The parent node of this node's parent, if existed. Otherwise return Tree.nil
 */
TreeNode * TreeNode::getGrandparent() const {
    if (isNull(this->getParent()))
        return nullptr;

    return this->getParent()->getParent();
}


int TreeNode::blackHeight() const {
    int height = 0;
    TreeNode * node = this->getParent();
    while (!isNull(node)) {
        if (node->colorIs(BLACK))
            height++;

        node = node->getParent();
    }

    return height;
}


void TreeNode::setNodePosID(int nodePosID) {
    this->nodePosID = nodePosID;
}


void TreeNode::setNodeID(int nodeID) {
    this->nodeID = nodeID;
}


void TreeNode::setName(std::string name) {
    TreeNode::name = name;
}


void TreeNode::setAlter(int alter) {
    TreeNode::alter = alter;
}


void TreeNode::setEinkommen(double einkommen) {
    TreeNode::einkommen = einkommen;
}


void TreeNode::setPLZ(int plz) {
    TreeNode::plz = plz;
}


void TreeNode::setRed(bool color) {
    this->color = color;
}


void TreeNode::setLeft(TreeNode *left) {
    this->left = left;
}


void TreeNode::setRight(TreeNode *right) {
    this->right = right;
}


void TreeNode::setParent(TreeNode *parent) {
    this->parent = parent;
}


void TreeNode::print() const {
    const int SPACE_ID = 6;
    const int SPACE_NAME = 21;
    const int SPACE_AGE = 4;
    const int SPACE_INCOME = 18;
    const int SPACE_PLZ = 6;
    const int SPACE_POS = 7;
    const int SPACE_COLOR = 7;
    std::cout << this->nodeID;

    int spaceNeeded = SPACE_ID - std::to_string(this->nodeID).length();
    fillSpace(spaceNeeded);

    std::cout << "| " << this->name;
    spaceNeeded = SPACE_NAME - this->name.length();
    fillSpace(spaceNeeded);

    std::cout << "|";
    spaceNeeded = SPACE_AGE - std::to_string(this->alter).length();
    fillSpace(spaceNeeded);
    std::cout << this->alter << " ";

    std::cout << "|";
    spaceNeeded = SPACE_INCOME - std::to_string(this->einkommen).length();
    fillSpace(spaceNeeded);
    std::cout << this->einkommen << " ";


    std::cout << "|";
    spaceNeeded = SPACE_PLZ - std::to_string(this->plz).length();
    fillSpace(spaceNeeded);
    std::cout << this->plz << " ";

    std::cout << "|";
    spaceNeeded = SPACE_POS - std::to_string(this->nodePosID).length();
    fillSpace(spaceNeeded);
    std::cout << this->nodePosID << " ";

    std::string color = (this->colorIs(RED)) ? "Red" : "Black";
    std::cout << "| ";
    spaceNeeded = SPACE_COLOR - color.length();
    fillSpace(spaceNeeded);
    std::cout << color << "\n";


}


bool TreeNode::isLeftChild() const {
    return this->getParent()->getLeft() == this;
}


bool TreeNode::isRightChild() const {
    return this->getParent()->getRight() == this;
}


/**
 * Change color a non-nil node (red -> black, black -> red)
 */
void TreeNode::recolor() {
    this->setColor(!this->color);
}


/**
 * Rotate this node to the left. Note that it should be able to rotate left (has right child). This condition should be
 * checked before calling this function.
 */
void TreeNode::rotateLeft() {
    TreeNode * rightChild = this->getRight();
    TreeNode * leftGrandchild = rightChild->getLeft();

    // old child become new parent
    rightChild->setParent(this->parent);

    if (!isNull(this->getParent())) // omit if this is root
        if (this->isLeftChild()) // change this' parent
            this->getParent()->setLeft(rightChild);
        else
            this->getParent()->setRight(rightChild);

    // left grandchild become this' new right child
    this->setRight(leftGrandchild);

    if (!isNull(leftGrandchild))
        leftGrandchild->setParent(this);

    // this become old child's new left child
    rightChild->setLeft(this);
    this->setParent(rightChild);
}


/**
 * Rotate this node to the left. Note that it should be able to rotate left (has right child). This condition should be
 * checked before calling this function.
 */
void TreeNode::rotateRight() {
    TreeNode * leftChild = this->getLeft();
    TreeNode * rightGrandchild = leftChild->getRight();

    // old child become new parent
    leftChild->setParent(this->parent);

    if (!isNull(this->getParent())) // omit if this is root
        if (this->isLeftChild()) // change this' parent
            this->getParent()->setLeft(leftChild);
        else
            this->getParent()->setRight(leftChild);

    // left grandchild become this' new left child
    this->setLeft(rightGrandchild);

    if (!isNull(rightGrandchild))
        rightGrandchild->setParent(this);

    // this become old child's new right child
    leftChild->setRight(this);
    this->setParent(leftChild);
}


/**
 * Compare 2 node's NodePosIDs
 */
bool operator<(const TreeNode & node1, const TreeNode & node2) {
    return node1.getNodePosID() < node2.getNodePosID();
}


/**
 * Compare 2 node's NodePosIDs
 */
bool operator>(const TreeNode & node1, const TreeNode & node2) {
    return node1.getNodePosID() > node2.getNodePosID();
}


/**
 * Print space n times.
 */
void fillSpace(int n) {
    for (int i = 0; i < n; i++) {
        std::cout << " ";
    }
}


bool isNull(TreeNode * node) {
    return (node == nullptr);
}
//
////////////////////////////////////