/*************************************************
* ADS Praktikum 2.2
* TreeNode.h
* Erweiterung um Hilfsattribute und -funktionen gestattet, wenn erforderlich.
*************************************************/
#ifndef TREE_NODE
#define TREE_NODE
#include <string>
#include "TreeNodeException.h"

using namespace std;

#define RED true
#define BLACK false

class TreeNode {

	
private:
    ///////////////////////////////////////
    // Ihr Code hier:


    int nodePosID;


    int nodeID;


    std::string name;


    int alter;


    double einkommen;


    int plz;


    TreeNode * left = nullptr;


    TreeNode * right = nullptr;


    TreeNode * parent = nullptr;


    bool color;


    void rotateLeft();


    void rotateRight();


    void setNodePosID(int nodePosID);


    void setNodeID(int nodeID);

    /**
     * Renamed functions to improve readability : setColor = setRed.
     */
    void setColor(bool color) {
        setRed(color);
    }


    bool colorIs(bool color) const {
        return this->color == color;
    }


    //
    ///////////////////////////////////////

public:
    ///////////////////////////////////////
    // Ihr Code hier:

    TreeNode(int nodePosId, int nodeId, string &name, int alter, double einkommen, int plz);


    int getNodePosID() const;


    int getNodeID() const;


    std::string getName() const;


    int getAlter() const;


    double getEinkommen() const;


    int getPLZ() const;


    TreeNode *getLeft() const;


    TreeNode *getRight() const;


    TreeNode *getParent() const;


    TreeNode *getUncle() const;


    TreeNode *getGrandparent() const;


    bool getRed() const;


    void setName(string name);


    void setAlter(int alter);


    void setEinkommen(double einkommen);


    void setPLZ(int plz);


    void setLeft(TreeNode *left);


    void setRight(TreeNode *right);


    void setParent(TreeNode *parent);


    void setRed(bool isRed);


    void recolor();


    void print() const;


    bool isLeftChild() const;


    bool isRightChild() const;


    int blackHeight() const;


    friend bool operator< (const TreeNode & node1, const TreeNode & node2);


    friend bool operator> (const TreeNode & node1, const TreeNode & node2);


    friend class Tree;

    //
    ///////////////////////////////////////
};


bool isNull(TreeNode * node);

#endif