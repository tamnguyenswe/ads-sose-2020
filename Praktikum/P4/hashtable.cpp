#include "hashtable.h"

using namespace std;

HashTable::HashTable(int size) {
	
    this->size = size;
    this->elements = 0;
    this->collisionCount = 0;
    this->hashTable = new vector<int>;

    for (int i = 0; i < size; i++)
        this->hashTable->push_back(-1);
}

HashTable::~HashTable()
{
	delete hashTable;
}

int HashTable::hashValue(int item) {
    int index = item % this->size;

	return index;
}

int HashTable::insert(int item) {
	
    int index = hashValue(item);

    int collisionHere = 0;
    while(this->hashTable->at(index) != -1) { // repeat until find an empty spot
        // if this spot is empty, collisionHere = 0, index = index
        collisionHere++;
        index = (hashValue(item) + (int) pow(collisionHere, 2)) % this->size;
    }

    if (collisionHere != 0) // if collision happens when inserting this item
        collisionCount += collisionHere;

    this->hashTable->at(index) = item;

	return 0; //dummy return
}


int HashTable::at(int i) {
	return hashTable->at(i);
}

int HashTable::getCollisionCount() {
	return this->collisionCount;
}

int HashTable::getSize() {
	return this->size;
}

int HashTable::getElements() {
	return this->elements;
}
