#include "sorting.h"


namespace sorting {

    //************
    // QuickSort *
    //************

    void QuickSort(vector<int> &arr, int left, int right) {

        if ((right < 0) ||
            (left < 0) ||
            (right - left < 1))
            return;

        int pivot = arr[right];
        int firstBig = left; //first bigger-than-pivot elem index. firstBig - 1 = lastSmall
        int firstEqual = left;

        for (int i = left; i < right; i++) {
            if (arr[i] < pivot) {
                swap(arr[i], arr[firstBig]);
                firstBig++;
                firstEqual++;
            } else if (arr[i] == pivot) {
                swap(arr[i], arr[firstBig]);
                firstBig++;
            }
        }

        swap(arr[right], arr[firstBig]);
        firstBig++;

        QuickSort(arr, left, firstEqual - 1);
        QuickSort(arr, firstBig, right);
    }

    //************
    // MergeSort *
    //************

    void Merge(vector<int> &a, vector<int> &b, int left, int mid, int right) {
        const int leftSize = mid - left + 1;
        const int rightSize = right - mid;
        int * leftTemp = new int[leftSize];
        int * rightTemp = new int[rightSize];

        for (int i = 0; i < leftSize; i++)
            leftTemp[i] = a[left + i];

        for (int i = 0; i <= rightSize; i++)
            rightTemp[i] = a[mid + i + 1];

        int iLeft = 0, iRight = 0, iMain = left;

        while ((iLeft < leftSize) &&
               (iRight < rightSize)) {
            if (leftTemp[iLeft] < rightTemp[iRight]) {
                a[iMain] = leftTemp[iLeft];
                iLeft++;
            } else {
                a[iMain] = rightTemp[iRight];
                iRight++;
            }
            iMain++;
        }

        for (; iLeft < leftSize; iLeft++)
            a[iMain++] = leftTemp[iLeft];

        for (; iRight < rightSize; iRight++)
            a[iMain++] = rightTemp[iRight];

    }

    /**
     * Merge sort an int vector
     * @param a Vector to sort
     * @param b Can be anything, I don't need this variable
     * @param left Left end to sort with
     * @param right Right end to sort with
     */
    void MergeSort(vector<int> &a, vector<int> &b, int left, int right) {
        if (left < right) {
            int mid = (left + right) / 2;

            MergeSort(a, b, left, mid);
            MergeSort(a, b, mid + 1, right);

            Merge(a, b, left, mid, right);

        }
    }



    //************
    // Heapsort  *
    //************

    /**
     * Build a max heap
     * @param a Array to build a heap from
     * @param thisIndex Current element's index
     */
    void heapify(vector<int> &a, const int & HEAP_SIZE, int thisIndex) {
        int indexMax = thisIndex;
        int left = 2 * thisIndex + 1; // left node
        int right = 2 * thisIndex + 2; // right node

        bool leftExist = left < HEAP_SIZE;
        bool rightExist = right < HEAP_SIZE;

        // find max in 3 nodes
        if (leftExist && (a[left] > a[indexMax]))
            indexMax = left;

        if (rightExist && (a[right] > a[indexMax]))
            indexMax = right;

        // if this node isn't the biggest, swap it with the biggest to maintain max heap
        if (indexMax != thisIndex) {
            swap(a[thisIndex], a[indexMax]);

            heapify(a, HEAP_SIZE, indexMax);
        }
    }

    /**
     * Heap sort a vector
     * @param a Vector to sort
     * @param n Vector's size
     */
    void HeapSort(vector<int> &a, int n) {
        int heapSize = a.size();

        // build a max heap from a
        for (int i = heapSize / 2 - 1; i >= 0 ; i--)
            heapify(a, heapSize, i);

        // sort a with this max heap
        for (int i = heapSize - 1; i >= 0; i--) {
            swap(a[0], a[i]); // swap the biggest (root) to last elem
            heapify(a, i, 0); // heapify with heap size = i, start at root
        }

    }



    //************
    // Shellsort *
    //************
    void ShellSort(vector<int> &a, int n) {
        n = a.size();

        int hibbard = pow(2, (int) log2(n)) - 1; // find max hibbard for this array

        while (true) {
            for (int shift = 0; shift < hibbard; shift++)
                for (int i = hibbard + shift; i < n; i += hibbard) {
                    int j = i;
                    int j_prev = j - hibbard;

                    while (a[j] < a[j_prev]) {
                        swap(a[j], a[j_prev]);
                        j -= hibbard;
                        j_prev = j - hibbard;

                        if (j_prev < 0)
                            break;
                    }
                }

            if (hibbard == 1)
                break; // break after hibbard = 1

            hibbard = (hibbard + 1) / 2 - 1; // gradually reduce hibbard
        }

    }


    void randomizeVector(vector<int> &array, int n) {

        array.clear();
        array.resize(n);

        for (int i = 0; i < array.size() ;i++)
            array[i]= rand() % 1000000;
    }


    void swap(int &num1, int &num2) {
        int temp = num1;
        num1 = num2;
        num2 = temp;
    }

}





