import matplotlib.pyplot as plt

param_amount = []
result_quick = []
result_merge = []
result_shell = []
result_heap  = []

def read_file(filename, data_set):
    with open(filename) as f:
        for line in f.readlines():
            try:
                data_set.append(float(line.split()[1]))
            except IndexError:
                pass #last empty line cannot be splitted

for i in range(1000, 1000001, 1000):
    param_amount.append(i);

read_file("heapsort.txt", result_heap)
read_file("mergesort.txt", result_merge)
read_file("shellsort.txt", result_shell)
read_file("quicksort.txt", result_quick)

plt.plot(param_amount, result_heap, label = "Heapsort")
plt.plot(param_amount, result_merge, label = "Mergesort")
plt.plot(param_amount, result_quick, label = "Quicksort")
plt.plot(param_amount, result_shell, label = "Shellsort")

plt.xlabel("Parameter amount")
plt.ylabel("Time(s)")
plt.ticklabel_format(axis = 'x', style = 'plain')
plt.legend(loc = "upper left")

plt.savefig("figure.png")