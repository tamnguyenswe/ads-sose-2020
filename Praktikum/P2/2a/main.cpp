/*************************************************
* ADS Praktikum 2.1
* main.cpp
*
//*************************************************/
#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include <iostream>
#include <string>
#include "Ring.h"

using namespace std;

#define COMMAND_ADD 1
#define COMMAND_SEARCH 2
#define COMMAND_PRINT 3
#define COMMAND_QUIT 4


void addNewBackup(Ring & ring) {
    std::string description, data;

    std::cout << "Adding a new backup..\n";

    std::cout << "New backup's description: ";
    std::cin >> description;

    std::cout << "New backup's data: ";
    std::cin >> data;

    ring.addNode(description, data);
}

void searchRing(Ring ring) {
    std::string keyword;

    std::cout << "Searching in backups...";
    std::cout << "Keyword to search with: ";

    std::cin >> keyword;

    ring.search(keyword);
}

int main()
{
	int result = Catch::Session().run();
	// Ihr Code hier:
//
    Ring ring = Ring();
    std::cout << "================================\n";
    std::cout << "Command list: 1 = Add backups, 2 = Search in data, 3 = View all backups, 4 = Quit\n";

    bool quitConfirm = false;
    while (!quitConfirm) {
        int command;

        try {
            std::cout << "\n\nInsert command: ";
            std::cin >> command;
            std::cout << "\n";
        } catch (exception E) {
            continue;
        }

        switch (command) {
            case COMMAND_ADD:
                addNewBackup(ring);
                break;

            case COMMAND_SEARCH:
                searchRing(ring);
                break;

            case COMMAND_PRINT:
                ring.print();
                break;

            case COMMAND_QUIT:
                quitConfirm = true;
                break;

            default:
                continue;
        }
    }



	//
	///////////////////////////////////////
//	system("Pause");
	return 0;
}
