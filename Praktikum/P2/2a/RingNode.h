/*************************************************
* ADS Praktikum 2.1
* RingNode.h
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
#pragma once
#include <string>
#include <iostream>

class RingNode{
	
private:
	// Ihr Code hier:

    int oldAge;

	std::string description;

	std::string symbolicData;

	RingNode * next;

	bool isAgeSet;

	bool isDescriptionSet;

	bool isDataSet;

	bool isNextSet;

	//	
public:
	// Ihr Code hier:

    RingNode();

	RingNode(int, std::string description, std::string data);

	bool isValid() const;

	int getAge() const;

	void setAge(int);

	std::string getDescription() const;

	void setDescription(std::string);

	std::string getData() const;

	void setData(std::string);

	RingNode * getNext() const;

	void * setNext(RingNode *);

	//
};

std::ostream& operator<< (std::ostream & out, const RingNode & node);
std::ostream& operator<< (std::ostream & out, const RingNode * node);