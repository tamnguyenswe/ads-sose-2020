/*************************************************
* ADS Praktikum 2.1
* Ring.h
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
// Ring .h
#pragma once
#include <string>
//#include "catch.hpp"
#include "RingNode.h"

class Ring{
	
	private:
		// Ihr Code hier:

        int AnzahlNodes;

		/**
		 * Anchor points at the newest elements
		 */
		RingNode * anker;

		RingNode * findLast();

		//
		////////////////////////////////////
	public:
		// Ihr Code hier:

		Ring();

		void addNode(std::string, std::string);

		bool search(std::string) const;

		void print() const;

		//
		////////////////////////////////////
		// friend Funktionen mussen unangetastet bleiben!
		friend RingNode * get_anker(Ring& r);
		friend int get_AnzahlNodes(Ring& r);
};
