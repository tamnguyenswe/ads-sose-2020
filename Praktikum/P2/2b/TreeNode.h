/*************************************************
* ADS Praktikum 2.2
* TreeNode.h
* Erweiterung um Hilfsattribute und -funktionen gestattet, wenn erforderlich.
*************************************************/
#pragma once
#include <string>

using namespace std;

class Tree;

class TreeNode {
	
private:

    int nodePosID;

    int nodeID;

    std::string name;

    ///////////////////////////////////////
    // Ihr Code hier:

    int alter;

    double einkommen;

    int plz;

    TreeNode * left = nullptr;

    TreeNode * right = nullptr;

    TreeNode * parent = nullptr;

    //
    ///////////////////////////////////////

public:
    ///////////////////////////////////////
    // Ihr Code hier:

    TreeNode(int nodePosId, int nodeId, string &name, int alter, double einkommen, int plz);


    int getNodePosID() const;


    int getNodeID() const;


    std::string getName() const;


    int getAlter() const;


    double getEinkommen() const;


    int getPLZ() const;


    TreeNode *getLeft() const;


    TreeNode *getRight() const;


    TreeNode *getParent() const;


    void setNodePosID(int nodePosID);


    void setNodeID(int nodeID);


    void setName(string name);


    void setAlter(int alter);


    void setEinkommen(double einkommen);


    void setPLZ(int plz);


    void setLeft(TreeNode *left);


    void setRight(TreeNode *right);


    void setParent(TreeNode *parent);


    void print();


    bool isFull();


    bool isEmpty();


    bool operator< (TreeNode other) const;


    bool operator> (TreeNode other) const;


    //
    ///////////////////////////////////////
};
