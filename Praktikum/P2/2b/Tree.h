/*************************************************
* ADS Praktikum 2.2
* Tree.h
* Erweiterung um Hilfsattribute und -funktionen gestattet, wenn erforderlich.
*************************************************/
#pragma once
#include <string>
#include "TreeNode.h"
#include "catch.h"

using namespace std;

class Tree {

private:
    ///////////////////////////////////////
    // Ihr Code hier:


    TreeNode * anker;


    int nodeIDCounter;


    int nodeCounter;


    int nodePosID(int alter, int plz, double einkommen);


    void insertNodeRecursive(TreeNode * child, TreeNode * parent);


    void preOrderPrint(TreeNode * node) const;


    bool isNameFound(const std::string & name, TreeNode * node) const;


    TreeNode * searchNode(const int &nodePosID, TreeNode * node) const;


    TreeNode * findMin(TreeNode * node) const;

//    TreeNode * searchBranch(const int& nodePosID, TreeNode * node) const;

    //
    ////////////////////////////////////

public:
    ///////////////////////////////////////
    // Ihr Code hier:

    Tree();


    void addNode(std::string name, int alter, double einkommen, int plz);


    bool deleteNode(int nodePosID);


    bool searchNode(const std::string & name) const;


    void printAll() const;

    //
    ////////////////////////////////////
    // friend-Funktionen sind f�r die Tests erforderlich und m�ssen unangetastet bleiben!
    friend TreeNode * get_anker(Tree& TN);
};
