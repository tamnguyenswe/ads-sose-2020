/*************************************************
* ADS Praktikum 2.2
* TreeNode.cpp
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
#include "TreeNode.h"
#include <iostream>
#include <string>


////////////////////////////////////
// Ihr Code hier:

void fillSpace(int n);

TreeNode::TreeNode(int nodePosId, int nodeId, std::string &name, int alter, double einkommen, int plz) :
    nodePosID(nodePosId), nodeID(nodeId) {

    this->name = name;
    this->alter = alter;
    this->einkommen = einkommen;
    this->plz = plz;
}


int TreeNode::getNodePosID() const {
    return nodePosID;
}


int TreeNode::getNodeID() const {
    return nodeID;
}


std::string TreeNode::getName() const {
    return name;
}


int TreeNode::getAlter() const {
    return alter;
}


double TreeNode::getEinkommen() const {
    return einkommen;
}


int TreeNode::getPLZ() const {
    return plz;
}


TreeNode *TreeNode::getLeft() const {
    return left;
}


TreeNode *TreeNode::getRight() const {
    return right;
}


TreeNode * TreeNode::getParent() const {
    return this->parent;
}


void TreeNode::setNodePosID(int nodePosID) {
    this->nodePosID = nodePosID;
}


void TreeNode::setNodeID(int nodeID) {
    this->nodeID = nodeID;
}


void TreeNode::setName(std::string name) {
    TreeNode::name = name;
}


void TreeNode::setAlter(int alter) {
    TreeNode::alter = alter;
}


void TreeNode::setEinkommen(double einkommen) {
    TreeNode::einkommen = einkommen;
}


void TreeNode::setPLZ(int plz) {
    TreeNode::plz = plz;
}


void TreeNode::setLeft(TreeNode *left) {
    this->left = left;
}


void TreeNode::setRight(TreeNode *right) {
    this->right = right;
}


void TreeNode::setParent(TreeNode *parent) {
    this->parent = parent;
}


void TreeNode::print() {
    const int SPACE_ID = 6;
    const int SPACE_NAME = 21;
    const int SPACE_AGE = 4;
    const int SPACE_INCOME = 18;
    const int SPACE_PLZ = 6;
    const int SPACE_POS = 7;
    std::cout << this->nodeID;

    int spaceNeeded = SPACE_ID - std::to_string(this->nodeID).length();
    fillSpace(spaceNeeded);

    std::cout << "| " << this->name;
    spaceNeeded = SPACE_NAME - this->name.length();
    fillSpace(spaceNeeded);

    std::cout << "|";
    spaceNeeded = SPACE_AGE - std::to_string(this->alter).length();
    fillSpace(spaceNeeded);
    std::cout << this->alter << " ";

    std::cout << "|";
    spaceNeeded = SPACE_INCOME - std::to_string(this->einkommen).length();
    fillSpace(spaceNeeded);
    std::cout << this->einkommen << " ";


    std::cout << "|";
    spaceNeeded = SPACE_PLZ - std::to_string(this->plz).length();
    fillSpace(spaceNeeded);
    std::cout << this->plz << " ";

    std::cout << "|";
    spaceNeeded = SPACE_POS - std::to_string(this->nodePosID).length();
    fillSpace(spaceNeeded);
    std::cout << this->nodePosID << "\n";
}


bool TreeNode::isFull() {
    return ((this->left != nullptr) && (this->right != nullptr));
}


bool TreeNode::isEmpty() {
    return ((this->left == nullptr) && (this->right == nullptr));
}


/**
 * Compare 2 node's NodePosIDs
 */
bool TreeNode::operator<(TreeNode other) const {
    return this->getNodePosID() < other.getNodePosID();
}


/**
 * Compare 2 node's NodePosIDs
 */
bool TreeNode::operator>(TreeNode other) const {
    return this->getNodePosID() > other.getNodePosID();
}


/**
 * Print space n times.
 */
void fillSpace(int n) {
    for (int i = 0; i < n; i++) {
        std::cout << " ";
    }
}
//
////////////////////////////////////