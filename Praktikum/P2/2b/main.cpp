/*************************************************
* ADS Praktikum 2.2
* main.cpp
*
*************************************************/
#define CATCH_CONFIG_RUNNER
#include "catch.h"
#include <iostream>
#include <fstream>
#include "Tree.h"

using namespace std;

///////////////////////////////////////
// Hilfsmethoden f�rs Men� hier:
#define CMD_INSERT_MANUAL 1
#define CMD_IMPORT_CSV 2
#define CMD_DELETE_RECORD 3
#define CMD_PRINT_ALL 4
#define CMD_DROP_TABLE 5
#define CMD_SEARCH 6
#define CMD_SHOW_COMMAND_LIST 9
#define CMD_QUIT 0

void insertRecord(Tree * tree) {
    std::cout << "Inserting a new record...\n";

    int age, plz;
    std::string name;
    double income;

    std::cout << "      Name: ";
    std::cin.ignore();
    std::getline(std::cin, name);
    std::cout << "      Age: ";
    std::cin >> age;
    std::cout << "      Income: ";
    std::cin >>income;
    std::cout << "      PLZ: ";
    std::cin >> plz;

    tree->addNode(name, age, income, plz);

    std::cout << "New record added successfully.\n";
}


void importCSV(Tree * tree) {
    std::cout << "Importing data from 'data.csv'..\n";

    //using clion, it seems like current directory is "cmake-build-debug" folder, rather than the project folder
    std::ifstream csvFile("../data.csv");

    std::string rawStr;

    while(!csvFile.eof()) {
        int age, plz;
        std::string name;
        double income;
        try {
            std::getline(csvFile, rawStr, ';');
            name = rawStr;
            std::getline(csvFile, rawStr, ';');
            age = std::stoi(rawStr);
            std::getline(csvFile, rawStr, ';');
            income = std::stod(rawStr);
            std::getline(csvFile, rawStr);
            plz = std::stoi(rawStr);
        } catch (std::invalid_argument &e) { // empty lines
            break;
        }

        tree->addNode(name, age, income, plz);
    }

    std::cout << "Data imported sucessfully.\n";
}


void deleteRecord(Tree * tree) {
    std::cout << "Choose one record to delete.\n";
    std::cout << "      Record PosID: ";

    int nodePosID;
    std::cin >> nodePosID;

    if (tree->deleteNode(nodePosID))
        std::cout << "Record " << nodePosID << " was successfully deleted.\n";
    else
        std::cout << "Cannot find this record in database\n";

}


void search(Tree * tree) {
    std::string name;

    std::cout << "Search a record with name.\n";
    std::cout << "      Record name: ";
    std::cin.ignore();
    std::getline(std::cin, name);

    if (tree->searchNode(name))
        std::cout << "Record " << name << " found.\n";
    else
        std::cout << "Cannot find this record in database\n";
}


void showCommandList() {
    std::cout << "===============================\n";
    std::cout << "Command list:\n";
    std::cout << "      1: Insert new record manually\n";
    std::cout << "      2: Import data from a CSV file\n";
    std::cout << "      3: Delete one record in database\n";
    std::cout << "      4: Show all records\n";
    std::cout << "      5: Drop table\n";
    std::cout << "      6: Search\n";
    std::cout << "      9: Show command list\n";
    std::cout << "      0: Quit\n";
}


void dropTable(Tree * &tree) {
    char command;
    std::cout << "You are about to drop the table. This will have consequences and cannot be reverted\n";
    while (true) {
        std::cout << "\nProceed? (y/n) : ";
        try {
            std::cin >> command;
        } catch (std::exception &e) {
            continue;
        }

        if (command == 'n') {
            std::cout << "Alright.. no table dropping then..\n";
            return;
        } else if (command == 'y') {
            std::cout << "This is a table, not a mic. Dropping it will really leave consequences.\n";
            while (true) {
                std::cout << "\nSeriously continue? (y/n) : ";
                try {
                    std::cin >> command;
                } catch (std::exception &e) {
                    continue;
                }

                if (command == 'n') {
                    std::cout << "Heh, what an inconsistent person. No table dropping then.\n";
                    return;
                } else if (command == 'y') {

                    delete tree;
                    tree = new Tree();
                    std::cout << "Table dropped. Don't blame me then. I've warned you\n";
                    return;

                } else
                    continue;
            }
        } else
            continue;
    }
}

//
///////////////////////////////////////
int main() {

	int result = Catch::Session().run();

///////////////////////////////////////
// Ihr Code hier:
    int command;
    Tree * tree = new Tree();

    showCommandList();

    while(true) {
        std::cout << "\nChoose command: ";

        try {
            std::cin >> command;
        } catch (std::exception &e) {
            continue;
        }

        switch (command) {
            case CMD_INSERT_MANUAL:
                insertRecord(tree);
                break;

            case CMD_IMPORT_CSV:
                importCSV(tree);
                break;

            case CMD_DELETE_RECORD:
                deleteRecord(tree);
                break;

            case CMD_PRINT_ALL:
                tree->printAll();
                break;

            case CMD_DROP_TABLE:
                dropTable(tree);
                break;

            case CMD_SEARCH:
                search(tree);
                break;

            case CMD_SHOW_COMMAND_LIST:
                showCommandList();
                break;

            case CMD_QUIT:
                return 0;

            default:
                continue;
        }
    }


//
///////////////////////////////////////
//	system("PAUSE");

}
