#ifndef _LIST_H
#define _LIST_H
#include "Node.h"
#include <iostream>
#include <string>

template <class T>
class List {
public:
private:
    struct form {
        std::string start = "<< ";
        std::string zwischen = ", ";
        std::string ende = " >>\n";
    } list_form;

    Node<T> * head_tail;                    // head_tail->next = first node, head_tail->prev = last node
    int list_size;                          // Länge der Kette
    bool temp;                              // normalerweise false; ist true, wenn es sich um eine temoräre Liste handelt
                                            // die innerhalb der überladenen Operatoren angelegt wird

    Node<T> * find_node(T key);           // find the first node contain that key

public:
    List();
    List(const List & _List);               // Kopie Konstruktor
    List(const List * _List);               // Kopie Konstruktor
    ~List();
    void insertFront(T key);                // Einfügen eines Knotens am Anfang
    void insertFront(List & _List);         // Einfügen einer vorhandenen Liste am Anfang
    void insertFront(List * _List);         // Einfügen einer vorhandenen Liste am Anfang
    void insertBack(T key);                 // Einfügen eines Knotens am Ende
    void insertBack(List & _List);          // Einfügen einer vorhandenen Liste am Ende
    void insertBack(List * _List);          // Einfügen einer vorhandenen Liste am Ende
    bool getFront(T & key);                 // Entnehmen eines Knoten am Anfang
    bool getBack(T & key);                  // Entnehmen eines Knoten am Ende
    bool del(T key);                        // löschen eines Knotens [key]
    bool search(T key);                     // Suchen eines Knoten
    bool swap(T key1, T key2);              // Knoten in der Liste vertauschen
    int size() const;                       // Größe der Lise (Anzahl der Knoten)
    bool test();                            // Überprüfen der Zeigerstruktur der Liste
    void format(const std::string & start, const std::string & zwischen, const std::string & ende);
                                            // Mit der format-Methode kann die Ausgabe gesteuert werden: operator <<

    List<T> & operator = (const List<T> & _List);                       // Zuweisungsoperator definieren
    List<T> & operator = (const List<T> * _List);                       // Zuweisungsoperator definieren
    List<T> & operator + (const List<T> & List_Append);                 // Listen zusammenführen zu einer Liste
    List<T> & operator + (const List<T> * List_Append);                 // Listen zusammenführen zu einer Liste

    template <typename Tf>
    friend std::ostream & operator << (std::ostream & stream, const List<Tf> & Liste);      // Ausgabeoperator überladen

    template <typename Tf>
    friend std::ostream & operator << (std::ostream & stream, const List<Tf> * Liste);      // Ausgabeoperator überladen
};


template <class T>
List<T>::List() {
    // Konstruktor für eine leere Liste
    head_tail = new Node<T>;
    list_size = 0;
    temp = false;
    head_tail->next = head_tail;
    head_tail->prev = head_tail;
}


template <class T>
List<T>::List(const List<T> & _List) {
    // Konstruktor mit Übergabe einer Liste, die dann kopiert wird.
    // in dem Objekt _List sind die Knotenwerte enthalten, die Kopiert werden sollen.
    list_form = _List.list_form;
    head_tail = new Node<T>;
    list_size = 0;
    temp = _List.temp;
    head_tail->next = head_tail;
    head_tail->prev = head_tail;
    head_tail->key = 0;

    Node<T> * tmp_node;
    tmp_node = _List.head_tail->next;
    while (tmp_node != _List.head_tail) {
        head_tail->prev = new Node<T>(tmp_node->key, head_tail->prev->next, head_tail->prev);
        head_tail->prev->prev->next = head_tail->prev;
        list_size++;
        tmp_node = tmp_node->next;
    }
    if (_List.temp) delete & _List;     // ist die Übergebene Liste eine temporäre Liste? -> aus Operator +
}


template <class T>
List<T>::List(const List<T> * _List) {
    // Konstruktor mit Übergabe einer Liste, die dann kopiert wird.
    // in dem Objekt _List sind die Knotenwerte enthalten, die Kopiert werden sollen.
    list_form = _List->list_form;
    head_tail = new Node<T>;
    list_size = 0;
    temp = _List->temp;
    head_tail->next = head_tail;
    head_tail->prev = head_tail;

    // copy all _List elements to this list
    Node<T> * tmp_node;
    tmp_node = _List->head_tail->next;
    while (tmp_node != _List->head_tail) {
        head_tail->prev = new Node<T>(tmp_node->key, head_tail->prev->next, head_tail->prev);
        head_tail->prev->prev->next = head_tail->prev;
        list_size++;
        tmp_node = tmp_node->next;
    }
    if (_List->temp) delete _List;      // ist die Übergebene Liste eine temporäre Liste? -> aus Operator +
}

template <class T>
List<T>::~List() {
    // Dekonstruktor
    // Alle Knoten der Liste müssen gelöscht werden, wenn die Liste gelöscht wird

    // only delete head tail if empty list
    if (this->list_size == 0) {
        delete this->head_tail;
        return;
    }

    // Start at head tail
    Node<T> * node = this->head_tail;


    // Detach last from head tail
    this->head_tail->prev->next = nullptr;

    while (node->next != nullptr) {
        node = node->next;
        delete node->prev;
    }

    // loop ends at the last elem
    delete node;
    list_size = 0;
}


template <class T>
void List<T>::insertFront(T key) {
    // Einfügen eines neuen Knotens am Anfang der Liste

    // link new 1 -> 2
    auto * new_first = new Node<T>(key, head_tail->next, head_tail);

    // relink 2 -> 1
    head_tail->next->prev = new_first;

    // link anchor -> 1
    head_tail->next = new_first;

    // increse size
    list_size++;
}


/**
 * Insert this list in front of another list
 * @param _List the other list
 */
template <class T>
void List<T>::insertFront(List<T> & _List) {
    // Einfügen einer vorhandenen Liste am Ende
    /*
    Die einzufügenden Knoten werden übernommen (nicht kopiert)
    Die einzufügende Liste _List ist anschließend leer.
    Es darf keine Schleife und kein new benutzt werden.
    */

    // Attach this's last to List' first
    this->head_tail->prev->next = _List.head_tail->next;
    _List.head_tail->next->prev = this->head_tail->prev;


    // Attach _List's last to this' head_tail
    this->head_tail->prev = _List.head_tail->prev;
    _List.head_tail->prev->next = this->head_tail;


    // Detach _List's head_tail
    _List.head_tail->next = _List.head_tail;
    _List.head_tail->prev = _List.head_tail;

    this->list_size += _List.list_size;
    _List.list_size = 0;

/*
    Es wird ein Objekt übergeben in dem Knoten vorhanden sein können.
    Diese Knoten (koplette Kette) werden an das Ende der Liste (this) angehangen ohne sie zu kopieren!
*/
}


/**
 * Insert this list in front of another list
 * @param _List the other list
 */
template <class T>
void List<T>::insertFront(List<T> * _List) {
    // Einfügen einer vorhandenen Liste am Ende
    /*
    Die einzufügenden Knoten werden übernommen (nicht kopiert)
    Die einzufügende Liste _List ist anschließend leer.
    Es darf keine Schleife und kein new benutzt werden.
    */

    // Attach this's last to List' first
    this->head_tail->prev->next = _List->head_tail->next;
    _List->head_tail->next->prev = this->head_tail->prev;


    // Attach _List's last to this' head_tail
    this->head_tail->prev = _List->head_tail->prev;
    _List->head_tail->prev->next = this->head_tail;


    // Detach _List's head_tail
    _List->head_tail->next = _List->head_tail;
    _List->head_tail->prev = _List->head_tail;

    this->list_size += _List->list_size;
    _List->list_size = 0;

/*
    Es wird ein Objekt übergeben in dem Knoten vorhanden sein können.
    Diese Knoten (koplette Kette) werden an das Ende der Liste (this) angehangen ohne sie zu kopieren!
*/
}


template <class T>
void List<T>::insertBack(T key) {
    // Einfügen eines neuen Knotens am Ende der Liste

    // link new last -> old last, new last -> head_tail
    auto * new_last = new Node<T>(key, head_tail, head_tail->prev);

    // relink old last -> new last
    new_last->prev->next = new_last;

    // link anchor -> new last
    head_tail->prev = new_last;

    // increse size
    list_size++;
}


/**
 * Insert this list at the back of another list
 * @param _List the other list
 */
template <class T>
void List<T>::insertBack(List<T> & _List) {
    // Einfügen einer vorhandenen Liste am Anfang
    /*
    Die einzufügenden Knoten werden übernommen (nicht kopiert)
    Die einzufügende Liste _List ist anschließend leer.
    Es darf keine Schleife und kein new benutzt werden.
    */

    // Attach _List's last to this' first
    this->head_tail->next->prev = _List.head_tail->prev;
    _List.head_tail->prev->next = this->head_tail->next;


    // Attach _List's first to this' head_tail
    this->head_tail->next = _List.head_tail->next;
    _List.head_tail->next->prev = this->head_tail;


    // Detach _List's head_tail
    _List.head_tail->next = _List.head_tail;
    _List.head_tail->prev = _List.head_tail;

    this->list_size += _List.list_size;
    _List.list_size = 0;
/*
    Es wird ein Objekt übergeben in dem Knoten vorhanden sein können.
    Diese Knoten (koplette Kette) werden an den Anfang der Liste (this) übertragen ohne sie zu kopieren!
*/
}


/**
 * Insert this list at the back of another list
 * @param _List the other list
 */
template <class T>
void List<T>::insertBack(List<T> * _List) {
    // Einfügen einer vorhandenen Liste am Anfang
    /*
    Die einzufügenden Knoten werden übernommen (nicht kopiert)
    Die einzufügende Liste _List ist anschließend leer.
    Es darf keine Schleife und kein new benutzt werden.
    */

    // Attach _List's last to this' first
    this->head_tail->next->prev = _List->head_tail->prev;
    _List->head_tail->prev->next = this->head_tail->next;


    // Attach _List's first to this' head_tail
    this->head_tail->next = _List->head_tail->next;
    _List->head_tail->next->prev = this->head_tail;


    // Detach _List's head_tail
    _List->head_tail->next = _List->head_tail;
    _List->head_tail->prev = _List->head_tail;

    this->list_size += _List->list_size;
    _List->list_size = 0;
/*
    Es wird ein Objekt übergeben in dem Knoten vorhanden sein können.
    Diese Knoten (koplette Kette) werden an den Anfang der Liste (this) übertragen ohne sie zu kopieren!
*/
}


template <class T>
bool List<T>::getFront(T & key) {
    // entnehmen des Knotens am Anfang der Liste
    // der Wert wird als Parameter zurückgegeben
    // der Knoten wird entnommen

    // return false if have nothing to return
    if (list_size == 0)
        return false;

    key = this->head_tail->next->key;

    // Attach 2nd to head tail
    Node<T> * new_first = this->head_tail->next->next;
    new_first->prev = this->head_tail;

    // delete old first
    delete this->head_tail->next;

    // Attach head tail to new first
    head_tail->next = new_first;

    list_size--;

    // return true if success
    return true;

/*
    Der Wert des vorderen Schlüsselknotens wird rückgegeben und der Knoten gelöscht.
    Die Methode del(key) darf nicht zum löschen benutzt werden.
*/
}


template <class T>
bool List<T>::getBack(T & key) {
    // entnehmen des Knotens am Ende der Liste
    // der Wert wird als Parameter zurückgegeben
    // der Knoten wird entnommen

    // return false if have nothing to return
    if (list_size == 0)
        return false;

    key = this->head_tail->prev->key;

    // Attach 2nd last to head tail
    Node<T> * new_last = this->head_tail->prev->prev;
    new_last->next = this->head_tail;

    // delete old last
    delete this->head_tail->prev;

    // Attach head tail to new last
    head_tail->prev= new_last;

    list_size--;

    // return true if success
    return true;

/*
    Der Wert des letzten Schlüsselknotens wird rückgegeben und der Knoten gelöscht.
    Die Methode del(key) darf nicht zum löschen benutzt werden.
*/
}


template <class T>
bool List<T>::del(T key) {
    // Löschen eines gegebenen Knotens

    Node<T> * target = this->find_node(key);

    // return false if target not found
    if (target == nullptr)
        return false;
    else {
        // link left to right
        target->prev->next = target->next;

        // link right to left
        target->next->prev = target->prev;

        // delete this node
        delete target;
        list_size--;
    }
/*
    Löschen des Knotens mit dem Schlüssel key
*/
    // return true if deleted success
    return true;
}


template <class T>
bool List<T>::search(T key) {
    // suchen eines Knotens

    Node<T> *target = this->find_node(key);

    // if target == null => key not found
    return (target != nullptr);
}


/**
 * Find the first node contain key
 * @param key They key to search with
 * @return The first node contain that key
 */
template <class T>
Node<T> * List<T>::find_node(T key) {

    // return null if empty list
    if (this->list_size == 0)
        return nullptr;

    // search for the target
    Node<T> *target = this->head_tail->next;

    while(target != this->head_tail) {
        if (target->key == key) {
            break;
        }

        target = target->next;
    }

    // return null if key not found
    if (target == head_tail) {
        return nullptr;
    } else {
        return target;
    }
}


template <class T>
bool List<T>::swap(T key1, T key2) {
    // Vertauschen von zwei Knoten
    // Dabei werden die Zeiger der Knoten und deren Nachbarn verändert.

    Node<T> * node1 = find_node(key1);
    Node<T> * node2 = find_node(key2);

    // return false if one of these two not found
    if ((node1 == nullptr) || (node2 == nullptr))
        return false;

    if (node1 == node2) {
        return true;
    }

    // if 2 nodes are adjacent
    if ((node1->next == node2) || (node2->next == node1)) {

        auto * node_left = (node1->next == node2) ? node1 : node2;
        auto * node_right = (node1->prev == node2) ? node1 : node2;

        // Reattach left & right to adjacent elems
        node_left->next = node_right->next;
        node_right->prev = node_left->prev;

        // Reattach adjacent elems to left & right
        node_left->next->prev = node_left;
        node_right->prev->next = node_right;

        // Swap left & right
        node_left->prev = node_right;
        node_right->next = node_left;
        return true;
    }


    // swap these 2 nodes' right
    auto * temp = node1->next;
    node1->next = node2->next;
    node2->next = temp;


    // swap these 2 nodes' left
    temp = node1->prev;
    node1->prev = node2->prev;
    node2->prev = temp;

    // reattach rights and lefts of these 2
    node1->next->prev = node1;
    node1->prev->next = node1;
    node2->next->prev = node2;
    node2->prev->next = node2;

/*
    Vertauschen von zwei Knoten mit dem key1 und dem key2
    Es dürfen nicht nur einfach die Schlüssel in den Knoten getauscht werden!
    Die Knoten sind in der Kette umzuhängen.
*/
    // return true if swap success
    return true;
}


template <class T>
int List<T>::size() const {
    // Rückgabe der Knoten in der Liste mit O(1)
    return list_size;
}


template <class T>
bool List<T>::test() {
    // Testmethode: die Methode durchläuft die Liste vom Anfang bis zum Ende und zurück
    // Es werden dabei die Anzahl der Knoten gezählt.
    // Stimmt die Anzahl der Knoten überein liefert die Methode true
    Node<T> * tmp = head_tail->next;
    int i_next = 0, i_prev = 0;
    while (tmp != head_tail) {
        tmp = tmp->next;
        if (i_next > list_size) return false;
        i_next++;
    }
    if (i_next != list_size) return false;
    tmp = head_tail->prev;
    while (tmp != head_tail) {
        tmp = tmp->prev;
        if (i_prev > list_size) return false;
        i_prev++;
    }
    return i_prev == i_next;
}


template <class T>
void List<T>::format(const std::string & start, const std::string & zwischen, const std::string & ende)
{
    list_form.start = start;
    list_form.zwischen = zwischen;
    list_form.ende = ende;
}

template <class T>
List<T> & List<T>::operator = (const List<T> & _List)
{
    // in dem Objekt _List sind die Knoten enthalten, die Kopiert werden sollen.
    // Kopiert wird in das Objekt "this"
    if (this == &_List) return *this;   //  !! keine Aktion notwendig
    Node<T> * tmp_node;
    if (list_size)
    {
        Node<T> * tmp_del;
        Node<T> * tmp_node = head_tail->next;
        while (tmp_node != head_tail)       // Alle eventuell vorhandenen Knoten in this löschen
        {
            tmp_del = tmp_node;
            tmp_node = tmp_node->next;
            delete tmp_del;
        }
        list_size = 0;
        head_tail->next = head_tail;
        head_tail->prev = head_tail;
    }
    tmp_node = _List.head_tail->next;
    while (tmp_node != _List.head_tail) {
        insertBack(tmp_node->key);
        tmp_node = tmp_node->next;
    }
    if (_List.temp) delete & _List;     // ist die Uebergebene Liste eine temporaere Liste? -> aus Operator +
    return *this;
}

template <class T>
List<T> & List<T>::operator = (const List<T> * _List)
{
    // in dem Objekt _List sind die Knoten enthalten, die Kopiert werden sollen.
    // Kopiert wird in das Objekt "this"
    if (this == _List) return *this;    //  !! keine Aktion notwendig
    Node<T> * tmp_node;
    if (list_size)  {
        Node<T> * tmp_del;
        tmp_node = head_tail->next;
        while (tmp_node != head_tail) {     // Alle eventuell vorhandenen Knoten in this loeschen

            tmp_del = tmp_node;
            tmp_node = tmp_node->next;
            delete tmp_del;
        }

        list_size = 0;
        head_tail->next = head_tail;
        head_tail->prev = head_tail;
    }
    tmp_node = _List->head->next;
    while (tmp_node != _List->tail)
    {
        InsertBack(tmp_node->key);
        tmp_node = tmp_node->next;
    }
    if (_List->temp) delete _List;      // ist die Uebergebene Liste eine temporaere Liste? -> aus Operator +
    return *this;
}

template <class T>
List<T> & List<T>::operator + (const List<T> & List_Append)
{
    Node<T> * tmp_node;
    List<T> * tmp;
    if (temp){                              // this ist eine temporaere Liste und kann veraendert werden
        tmp = this;
    }
    else {
        tmp = new List(this);               // this ist keine temporaere Liste -> Kopie erzeugen
        tmp->temp = true;                   // Merker setzten, dass es sich um eine temporaere Liste handelt
    }
    if (List_Append.list_size) {                // anhängen der uebergebenen Liste an tmp
        tmp_node = List_Append.head_tail->next;
        while (tmp_node != List_Append.head_tail){
            tmp->insertBack(tmp_node->key);
            tmp_node = tmp_node->next;
        }
    }
    if (List_Append.temp) delete & List_Append;     // wurde eine temporaere Liste uebergeben, dann wird diese geloescht
    return *tmp;
}

template <class T>
List<T> & List<T>::operator + (List<T> const * List_Append)
{
    Node<T> * tmp_node;
    List<T> * tmp;
    if (temp){                              // this ist eine temporaere Liste und kann veraendert werden
        tmp = this;
    }
    else {
        tmp = new List(this);               // this ist keine temporaere Liste -> Kopie erzeugen
        tmp->temp = true;                   // Merker setzten, dass es sich um eine temporaere Liste handelt
    }
    if (List_Append->list_size) {               // anhaengen der uebergebenen Liste an tmp
        tmp_node = List_Append->head_tail->next;
        while (tmp_node != List_Append->head_tail) {
            tmp->insertBack(tmp_node->key);
            tmp_node = tmp_node->next;
        }
    }
    if (List_Append->temp) delete List_Append;      // wurde eine temporaere Liste uebergeben, dann wird diese geloescht
    return *tmp;
}


template <class Tf>
std::ostream & operator << (std::ostream  & stream, const List<Tf> & Liste)
{
    stream << Liste.list_form.start;
    for (Node<Tf> * tmp = Liste.head_tail->next; tmp != Liste.head_tail; tmp = tmp->next)
        stream << tmp->key << (tmp->next == Liste.head_tail ? Liste.list_form.ende : Liste.list_form.zwischen);
    if (Liste.temp) delete & Liste;         // wurde eine temporaere Liste uebergeben, dann wird diese geloescht
    return stream;
}

template <class Tf>
std::ostream & operator << (std::ostream  & stream, const List<Tf> * Liste)
{
    stream << Liste->list_form.start;
    for (Node<Tf> * tmp = Liste->head_tail->next; tmp != Liste->head_tail; tmp = tmp->next)
        stream << tmp->key << (tmp->next == Liste->head_tail ? Liste->list_form.ende : Liste->list_form.zwischen);
    if (Liste->temp) delete Liste;          // wurde eine temporaere Liste uebergeben, dann wird diese geloescht
    return stream;
}


#endif